from __future__ import print_function
import sys
import json
import ast
import psutil
from PyQt4 import QtGui, QtCore, uic
from datetime import datetime


def p(x):
    print(x)


class MyWindow(QtGui.QMainWindow):

    def __init__(self):
        # initialize Login Window
        QtGui.QMainWindow.__init__(self)
        self.ui = uic.loadUi('./resources/PyChat.ui', self)
        self.setWindowIcon(QtGui.QIcon('./resources/window_logo.png'))
        self.ui.show()
        self.ui.exit_button.clicked.connect(
            QtCore.QCoreApplication.instance().quit)
        self.ui.connect_button.setAutoDefault(True)
        self.ui.connect_button.setFocus()
        self.ui.connect_button.clicked.connect(self.connectToServer)
        self.ui.host.returnPressed.connect(self.ui.connectToServer)
        self.ui.username.returnPressed.connect(self.ui.connectToServer)
        self.ui.port.returnPressed.connect(self.ui.connectToServer)
        self.process = QtCore.QProcess()

    def closeEvent(self, event):
        # notify server of closed connection
        event.ignore()
        self.process.write("/quit\n")
        self.process.waitForReadyRead(500)
        for process in psutil.process_iter():
            tmp = process.cmdline()
            if './resources/client.py' in tmp:
                print("Process found, terminating!")
                process.terminate()
        QtCore.QCoreApplication.quit()

    def connectToServer(self):
        # get login data from screen, connect to server
        port = str(self.ui.port.text()).rstrip()
        host = str(self.ui.host.text()).rstrip()
        username = str(self.ui.username.text()).rstrip()

        if ' ' not in username and 1024 < int(port) <= 65536:
            self.initChatroomWindow()
            self.ui.userlist.addItem(QtGui.QListWidgetItem(username))
            self.process.start(
                'python', ['./resources/client.py', host, port, username])

    def initChatroomWindow(self):
        # initialize chatroom
        self.process.readyReadStandardOutput.connect(self.stdoutReady)
        self.process.readyReadStandardError.connect(self.stderrReady)
        self.ui = uic.loadUi('./resources/chatroom.ui', self)
        self.ui.setWindowTitle("PyChat!")
        self.ui.send.clicked.connect(self.write)
        self.process.started.connect(lambda: p('Started!'))
        self.process.finished.connect(lambda: p('Finished!'))
        self.lineEdit.returnPressed.connect(self.ui.send.click)
        self.ui.textEdit.setStyleSheet("""QListWidget{background: #fdf6e3;}""")
        self.ui.userlist.setStyleSheet("""QListWidget{background: #fdf6e3;}""")

    def write(self):
        # write to sdtin of client process
        t = datetime.now()
        tmp = self.ui.lineEdit.text()
        self.process.write(unicode(tmp) + "\n")
        self.process.waitForReadyRead(500)
        self.ui.append(tmp, (38, 139, 210), "Me ",
                       "[%s:%s:%s]" % (t.hour, t.minute, t.second))
        self.lineEdit.setText("")

    def refreshUserList(self, user_list):
        self.ui.userlist.clear()
        x = ast.literal_eval(user_list)
        for l in x:
            self.ui.userlist.addItem(QtGui.QListWidgetItem(l))

    def append(self, text, color, name=None, time=None):
        # format input
        if name is not None:
            j = QtGui.QListWidgetItem("%s %s |" % (time, name))
            j.setTextAlignment(QtCore.Qt.AlignRight)
            j.setTextColor(QtGui.QColor(color[0], color[1], color[2]))
            self.ui.identifier.addItem(j)
        else:
            j = QtGui.QListWidgetItem("SERVER")
            j.setTextAlignment(QtCore.Qt.AlignRight)
            self.ui.identifier.addItem(j)

        i = QtGui.QListWidgetItem(text)
        i.setTextColor(QtGui.QColor(color[0], color[1], color[2]))
        self.ui.textEdit.addItem(i)

    def stdoutReady(self):
        # get stdout of client process
        text = unicode(QtCore.QString((self.process.readAllStandardOutput())))
        try:
            # incoming messages from users
            client_json = json.loads(text)
            if client_json['type'] == 'chat':
                name = client_json['username']
                time = client_json['time']
                self.append(client_json['content'],
                            (101, 123, 131), name, time)
            # incoming notifications from server
            elif client_json['type'] == 'userlist':
                self.refreshUserList(client_json['list'])
                msg = client_json['content']
                if 'entered' in msg:
                    self.append(msg, (133, 153, 0))
                else:
                    self.append(msg, (220, 50, 47))
                    print(msg)

        except Exception as inst:
            print(text)

    def stderrReady(self):
        # catch errors
        text = str(self.process.readAllStandardError())
        print(text.strip())


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    window = MyWindow()
    sys.exit(app.exec_())
