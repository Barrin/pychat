import sys
import socket
from threading import Thread
import time


class Client():

    def __init__(self, host, port, nickname):
        self.host = host
        self.port = port
        self.nickname = nickname

    def run(self):
        sys.exit(self.chat_client())

    def send_msg(self, sock):
        while True:
            sys.stdout.write('\n')
            msg = sys.stdin.readline()
            msg = msg.rstrip()
            sock.send("%s" % msg)
            sys.stdout.flush()

    def recv_msg(self, sock):
        # print data
        while True:
            sock.settimeout(None)
            data = sock.recv(4096)
            sys.stdout.write("%s" % data)
            sys.stdout.flush()

    def chat_client(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(2)

        # connect to remote host
        try:
            s.connect((self.host, self.port))
        except:
            print('Unable to connect')
            sys.exit()

        s.send(username)
        sys.stdout.flush()

        Thread(target=self.recv_msg, args=(s,)).start()
        time.sleep(0.3)
        Thread(target=self.send_msg, args=(s,)).start()


if __name__ == '__main__':

    host, port, username = sys.argv[1], int(sys.argv[2]), sys.argv[3]
    client1 = Client(host, port, username)
    client1.run()
