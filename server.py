import sys
import socket
import select
import time
from datetime import datetime


class Server():

    def __init__(self, host, port):
        self.HOST = host
        self.PORT = port
        self.SOCKET_LIST = []
        self.RECV_BUFFER = 4096
        self.user_list = []

    def generateUserList(self, user_list):
        for entry in user_list:
            if entry[0] not in self.SOCKET_LIST:
                self.user_list.remove(entry)
        users = [y[1] for y in self.user_list]
        return users

    def get_user_by_sock(self, sock):
        for entry in self.user_list:
            if entry[0] == sock:
                return entry[1]

    def broadcastUsers(self, mode, new_user, server, client):
            users = self.generateUserList(self.user_list)
            t = datetime.now()
            self.broadcast(server, client,
                           '{"type": "userlist", "list": "%s",'
                           ' "content": "[%s:%s] %s has %s the channel!"}' % (str(users), t.hour, t.minute, new_user, mode))

    def chat_server(self):

        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind((self.HOST, self.PORT))
        server_socket.listen(10)
        # add server socket object to the list of readable connections
        self.SOCKET_LIST.append(server_socket)

        print("Chat server started on port " + str(self.PORT))

        while 1:

            # get the list sockets which are ready to be read through select
            # 4th arg, time_out  = 0 : poll and never block
            ready_to_read, ready_to_write, in_error = select.select(self.SOCKET_LIST, [], [], 0)

            for sock in ready_to_read:
                # a new connection request received
                if sock == server_socket:
                    sockfd, addr = server_socket.accept()
                    self.SOCKET_LIST.append(sockfd)
                    username = sockfd.recv(self.RECV_BUFFER)
                    if (sockfd, username) not in self.user_list:
                        self.user_list.append((sockfd, username))

                    self.broadcastUsers("entered", username, server_socket, sock)

                # a message from a client, not a new connection
                else:
                    try:
                        # receiving data from the socket.
                        data = sock.recv(self.RECV_BUFFER)
                        if data:
                            if '/quit' in data:
                                print(True)
                                self.SOCKET_LIST.remove(sock)
                                self.broadcastUsers("left",
                                                    self.get_user_by_sock(sock), server_socket, sock)
                            else:
                                # there is something in the socket
                                t = datetime.now()
                                user = self.get_user_by_sock(sock)
                                message = '{"type": "chat", "time": "[%s:%s:%s]", "username": "%s",' \
                                          ' "content": "%s"}' % (t.hour, t.minute, t.second, user, data)
                                self.broadcast(server_socket, sock, message)
                        else:
                            # remove the socket that's broken
                            if sock in self.SOCKET_LIST:
                                print("Client (%s, %s) disconnected" % addr)
                                self.SOCKET_LIST.remove(sock)
                                # at this stage, no data means probably the connection has been broken
                                self.broadcastUsers("left",
                                                    self.get_user_by_sock(sock), server_socket, sock)
                    # exception
                    except:
                        self.broadcast(server_socket, sock, "Client (%s, %s) is offline\n" % addr)
                        continue

            for sock in ready_to_write:
                self.broadcast(server_socket, sock,
                               '{"type": "chat", "content": "Test"}')

            time.sleep(0.05)
        server_socket.close()

    # broadcast chat messages to all connected clients
    def broadcast(self, server_socket, sock, message):
        for connection in self.SOCKET_LIST:
            # send the message only to peer
            if connection != server_socket and connection != sock:
                try:
                    connection.send(message)
                except:
                    # broken socket connection
                    connection.close()
                    # broken socket, remove it
                    if connection in self.SOCKET_LIST:
                        self.SOCKET_LIST.remove(connection)


if __name__ == "__main__":

    server = Server('localhost', 9009)
    sys.exit(server.chat_server())
